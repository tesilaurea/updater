package main

import (
	"encoding/json"
	"log"
)

type Service struct {
	JavaClass    string  `json:"@class"`
	Uid          string  `json:"uid"`
	Type         string  `json:"type"`
	Node         string  `json:"node"`
	Ip           string  `json:"ip"`
	Speedup      float64 `json:"speedup"`
	Availability float64 `json:"availability"`
}

type ServiceList []Service

func makeService(id string, t string, node string, ip string, speedup float64, availability float64) Service {
	return Service{
		JavaClass:    "org.uniroma2.model.VertexConcrete",
		Uid:          id,
		Type:         t,
		Node:         node,
		Ip:           ip,
		Speedup:      speedup,
		Availability: availability,
	}
}

func (s Service) ToString() string {
	serialized, err := json.Marshal(s)
	if err == nil {
		log.Println("serialized data: ", string(serialized))
		//serialized data:  {"Sad":true,"Happy":{"Money":0,"Moral":false,"Health":false},"Confused":0}
		//do redis transactions...
	}
	return string(serialized)
}

func (sList ServiceList) GetServiceWithUid(uid string) Service {
	for _, s := range sList {
		if s.Uid == uid {
			return s
		}
	}
	panic("Fatal Error!!!!")
}

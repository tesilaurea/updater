package main

import (
	"encoding/json"
	_ "fmt"
	"io/ioutil"
	_ "log"
	"net/http"
	"strconv"
	_ "time"
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Ricezione abstract workflow
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func UpdateRedis(w http.ResponseWriter, r *http.Request) {

	var rstate Rstate
	//riceve un Json relativo all'elenco di tutti servizi concreti e dei loro Ip
	//e del loro
	body, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(body, &rstate); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
	}
	var service ServiceList
	for _, s := range rstate.Services {
		serv := makeService(s.Uid, s.Type, s.Node, s.Ip, s.Speedup, s.Availability)
		service = append(service, serv)
	}
	/*//service, matrix := simulate()
	matrix := make([][]int64, len(rstate.Services))

	for l := 0; l < len(rstate.Services); l++ {
		matrix[l] = make([]int64, len(rstate.Services))
	}*/

	connections := rstate.Latencies

	//Qui viene fatto l'update di Redis
	//for k := 0; k < len(rstate.Services); k++ {
	for k := 0; k < len(service); k++ {
		err := client.HSet("Resource", strconv.Itoa(k), service[k].ToString()).Err()
		if err != nil {
			panic(err)
		}
	}
	for _, h := range connections {
		ServiceFrom := service.GetServiceWithUid(h.UidFrom)
		ServiceTo := service.GetServiceWithUid(h.UidTo)
		e := makeEdge("("+h.UidFrom+","+h.UidTo+")", ServiceFrom, ServiceTo, h.Latency, 1.0)

		err := client.HSet("Link", "("+h.UidFrom+","+h.UidTo+")", e.ToString()).Err()
		if err != nil {
			panic(err)
		}
	}

	//pubblico sulla code update --> mi ricalcolo tutte le associazioni! E aggiorno il db

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
}

func UpdateTuRedis(w http.ResponseWriter, r *http.Request) {
	var microservices []MetaMicroservice
	body, _ := ioutil.ReadAll(r.Body)
	if err := json.Unmarshal(body, &microservices); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
	}
	for k := 0; k < len(microservices); k++ {
		redisMicroservice := makeRedisMicroservice(microservices[k].Uid,
			microservices[k].Type, microservices[k].Deployment, microservices[k].Name, microservices[k].Tu)
		err := client.HSet("Microservice", redisMicroservice.Uid+"|"+redisMicroservice.Type+"|"+redisMicroservice.Name, redisMicroservice.ToString()).Err()
		if err != nil {
			panic(err)
		}
	}
	// Devo salvarli su Redis!
}

func UpdateRedisSimulation(w http.ResponseWriter, r *http.Request) {

	service, matrix := simulate()
	for k := 0; k < len(service); k++ {
		err := client.HSet("Resource", strconv.Itoa(k), service[k].ToString()).Err()
		if err != nil {
			panic(err)
		}
	}
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			e := makeEdge("("+strconv.Itoa(i)+","+strconv.Itoa(j)+")", service[i], service[j], matrix[i][j], 1.0)
			err := client.HSet("Link", "("+strconv.Itoa(i)+","+strconv.Itoa(j)+")", e.ToString()).Err()
			if err != nil {
				panic(err)
			}
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
}

func UpdateRedisTuSimulation(w http.ResponseWriter, r *http.Request) {

	microservices := simulateTu()
	for k := 0; k < len(microservices); k++ {
		redisMicroservice := makeRedisMicroservice(microservices[k].Uid,
			microservices[k].Type, microservices[k].Deployment, microservices[k].Name, microservices[k].Tu)
		err := client.HSet("Microservice", redisMicroservice.Uid+"|"+redisMicroservice.Type+"|"+redisMicroservice.Name, redisMicroservice.ToString()).Err()
		if err != nil {
			panic(err)
		}
	}
}

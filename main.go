package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/gorilla/handlers"
	"log"
	"net/http"
)

/**
 * Main principale del client backend scritto in Go
 */

func main() {
	ExampleNewClient()
	port := ":8084"
	router := NewRouter()
	fmt.Println("Mi metto in ascolto!")
	log.Fatal(http.ListenAndServe(port, handlers.CORS()(router)))

}

func ExampleNewClient() {
	client = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(pong, err)
	// Output: PONG <nil>
}

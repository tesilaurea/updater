package main

import (
    "log"
    "net/http"
    "time"
)

/**
 * Funzione che fa da logger e consente di conoscere le richieste che sono state effettuate al microservizio, 
 * e il tempo di esecuzione nel caso esso sia lanciato in modalità non detached
 *
 */

func Logger(inner http.Handler, name string) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        start := time.Now()

        inner.ServeHTTP(w, r)

        log.Printf(
            "%s\t%s\t%s\t%s",
            r.Method,
            r.RequestURI,
            name,
            time.Since(start),
        )
    })
}
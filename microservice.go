package main

import (
	"encoding/json"
	"fmt"
	gocql "github.com/gocql/gocql"
	"io/ioutil"
	"log"
)

type Microservice struct {
	Name string `json:"name"`
	Tu   int    `json:"tu"`
}

func getMicroservice() []Microservice {
	raw, err := ioutil.ReadFile("./pages.json")
	if err != nil {
		fmt.Println(err.Error())
	}

	var c []Microservice
	json.Unmarshal(raw, &c)
	return c
}

type Microservices struct {
	Uid              string         `json:"uid"`
	Type             string         `json:"type"`
	Deployment       string         `json:"deployment"`
	MicroserviceList []Microservice `json:"microservices"`
}

type MetaMicroservice struct {
	UUid       gocql.UUID `json:"uuid"`
	Uid        string     `json:"uid"`
	Type       string     `json:"type"`
	Deployment string     `json:"deployment"`
	Name       string     `json:"name"`
	Tu         int        `json:"tu"`
}

func makeMetaMicroservice(uuid gocql.UUID, uid string, t string, deployment string, name string,
	tu int) MetaMicroservice {
	return MetaMicroservice{
		UUid:       uuid,
		Uid:        uid,
		Type:       t,
		Deployment: deployment,
		Name:       name,
		Tu:         tu,
	}
}

type RedisMicroservice struct {
	JavaClass  string `json:"@class"`
	Uid        string `json:"uid"`
	Type       string `json:"type"`
	Deployment string `json:"deployment"`
	Name       string `json:"name"`
	Tu         int    `json:"tu"`
}

func makeRedisMicroservice(id string, t string, deployment string, name string, tu int) RedisMicroservice {
	return RedisMicroservice{
		JavaClass:  "org.uniroma2.model.RedisMicroservice",
		Uid:        id,
		Type:       t,
		Deployment: deployment,
		Name:       name,
		Tu:         tu,
	}
}

func (s RedisMicroservice) ToString() string {
	serialized, err := json.Marshal(s)
	if err == nil {
		log.Println("serialized data: ", string(serialized))
		//serialized data:  {"Sad":true,"Happy":{"Money":0,"Moral":false,"Health":false},"Confused":0}
		//do redis transactions...
	}
	return string(serialized)
}

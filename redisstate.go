package main

type Rstate struct {
	Services  MetaServices  `json:"services"`
	Latencies MetaLatencies `json:"latencies"`
}

type MetaService struct {
	Uid          string  `json:"uid"`
	Type         string  `json:"type"`
	Node         string  `json:"node"`
	Ip           string  `json:"ip"`
	Availability float64 `json:"availability"`
	Speedup      float64 `json:"speedup"`
}

type MetaLatency struct {
	UidFrom string `json:"from"`
	UidTo   string `json:"to"`
	Latency int64  `json:"latency"`
}

type MetaServices []MetaService

type MetaLatencies []MetaLatency

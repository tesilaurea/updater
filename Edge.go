package main

import (
	"encoding/json"
	"log"
)

type Edge struct {
	JavaClass    string  `json:"@class"`
	Uid          string  `json:"uid"`
	From         Service `json:"from"`
	To           Service `json:"to"`
	Delay        int64   `json:"delay"`
	Availability float64 `json:"availability"`
}

func makeEdge(id string, from Service, to Service, delay int64, availability float64) Edge {
	return Edge{
		JavaClass:    "org.uniroma2.model.EdgeConcrete",
		Uid:          id,
		From:         from,
		To:           to,
		Delay:        delay,
		Availability: availability,
	}
}

func (e Edge) ToString() string {
	serialized, err := json.Marshal(e)
	if err == nil {
		log.Println("serialized data: ", string(serialized))
		//serialized data:  {"Sad":true,"Happy":{"Money":0,"Moral":false,"Health":false},"Confused":0}
		//do redis transactions...
	}
	return string(serialized)
}

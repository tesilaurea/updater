package main

import (
	gocql "github.com/gocql/gocql"
)

func simulate() ([]Service, [][]int64) {

	service0 := makeService("0", "Service0", "Node0", "192.168.1.0", 1.0, 0.6)
	service1 := makeService("1", "Service1", "Node1", "192.168.1.1", 1.0, 0.4)
	service2 := makeService("2", "Service2", "Node2", "192.168.1.2", 1.0, 0.8)
	service3 := makeService("3", "Service3", "Node0", "192.168.1.3", 1.0, 0.6)
	service4 := makeService("4", "Service4", "Node1", "192.168.1.4", 1.0, 0.9)
	service5 := makeService("5", "Service4", "Node0", "192.168.1.5", 1.0, 0.9)
	service6 := makeService("6", "Service3", "Node1", "192.168.1.6", 1.0, 0.1)
	service7 := makeService("7", "Service0", "Node2", "192.168.1.7", 1.0, 0.4)
	service8 := makeService("8", "Service4", "Node2", "192.168.1.8", 1.0, 0.7)
	service9 := makeService("9", "Service3", "Node2", "192.168.1.9", 1.0, 0.3)

	var service []Service
	service = []Service{
		service0, service1, service2,
		service3, service4, service5, service6, service7, service8, service9,
	}
	//Questa è la tabella delle latenze che ritorna dal servizio monitormanager
	matrix := [][]int64{
		{0, 25, 24, 0, 25, 0, 23, 16, 21, 13},
		{38, 0, 20, 24, 0, 19, 0, 23, 21, 28},
		{20, 22, 0, 30, 20, 16, 18, 0, 0, 0},
		{0, 28, 26, 0, 23, 0, 22, 24, 27, 11},
		{16, 0, 24, 21, 0, 22, 0, 30, 29, 20},
		{0, 24, 19, 0, 17, 0, 22, 19, 20, 24},
		{22, 0, 24, 16, 0, 19, 0, 24, 24, 19},
		{13, 13, 0, 18, 26, 22, 17, 0, 0, 0},
		{15, 27, 0, 29, 25, 18, 20, 0, 0, 0},
		{23, 25, 0, 30, 26, 18, 16, 0, 0, 0}}

	return service, matrix
}

func simulateTu() []MetaMicroservice {
	service0 := makeService("0", "Service0", "Node0", "192.168.1.0", 1.0, 1.0)
	service1 := makeService("1", "Service1", "Node1", "192.168.1.1", 1.0, 1.0)
	service2 := makeService("2", "Service2", "Node2", "192.168.1.2", 1.0, 1.0)
	service3 := makeService("3", "Service3", "Node0", "192.168.1.3", 1.0, 1.0)
	service4 := makeService("4", "Service4", "Node1", "192.168.1.4", 1.0, 1.0)
	service5 := makeService("5", "Service4", "Node0", "192.168.1.5", 1.0, 1.0)
	service6 := makeService("6", "Service3", "Node1", "192.168.1.6", 1.0, 1.0)
	service7 := makeService("7", "Service0", "Node2", "192.168.1.7", 1.0, 1.0)
	service8 := makeService("8", "Service4", "Node2", "192.168.1.8", 1.0, 1.0)
	service9 := makeService("9", "Service3", "Node2", "192.168.1.9", 1.0, 1.0)
	var service []Service
	service = []Service{
		service0, service1, service2,
		service3, service4, service5, service6, service7, service8, service9,
	}
	var microservices []MetaMicroservice

	for _, s := range service {
		microservice0 := makeMetaMicroservice(gocql.TimeUUID(), s.Uid, s.Type, "deployment", "microservice0",
			1)
		microservices = append(microservices, microservice0)

		microservice1 := makeMetaMicroservice(gocql.TimeUUID(), s.Uid, s.Type, "deployment", "microservice3/{url1}/vla",
			2)
		microservices = append(microservices, microservice1)
		microservice2 := makeMetaMicroservice(gocql.TimeUUID(), s.Uid, s.Type, "deployment", "microservice2",
			3)
		microservices = append(microservices, microservice2)
		microservice3 := makeMetaMicroservice(gocql.TimeUUID(), s.Uid, s.Type, "deployment", "microservice4/{url1}/vla",
			4)
		microservices = append(microservices, microservice3)

	}
	return microservices
}

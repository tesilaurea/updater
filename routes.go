package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

/**
 * Endpoint dell'applicazione, con i relativi metodi che vengono invocati
 *
 */

var routes = Routes{
	Route{
		"UpdateRedis",
		"POST",
		"/redis/service/update",
		UpdateRedis,
	},
	Route{
		"UpdateTuRedis",
		"POST",
		"/redis/microservices/update",
		UpdateTuRedis,
	},
	Route{
		"UpdateRedisSimulation",
		"GET",
		"/redis/service/sim/update",
		UpdateRedisSimulation,
	},
	Route{
		"UpdateRedisTuSimulation",
		"GET",
		"/redis/microservices/sim/update",
		UpdateRedisTuSimulation,
	},
}
